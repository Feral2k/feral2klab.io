# Logan Garcia's personal website

These are the source files for my personal website, which I currently use as the online version of my resume.

## Why don't you use an SSG (Static Site Generator)?

I built the first version of my website with Jekyll along with some theme I found for resumes. I was in the process of migrating the theme to Pelican (as I much prefer Python), but during that time, I realized that since it was only really two pages (one in English and the other in Esperanto), I just kept getting the feeling that using an SSG is overengineering. If I had a blog or something else where I would keep adding files that would need to share the same theme, I would definitely use something like Pelican.

## Why do you have files in .html and .xhtml extensions?

I need to have browsers serve my website in the MIME type `application/xhtml+xml` so that browsers can correctly validate it and serve it as XHTML. However, many simple static web hosting services (such as GitLab Pages) do not pick up `index.xhtml` files to serve them from the root directory automatically. To account for that, I create symlinks with the `.html` extension, which allows them to be served by these hosting services and in the correct MIME type.

## XHTML? What's that?

As I was developing the latest iteration of my website, I stumbled upon [XHTML](https://html.spec.whatwg.org/multipage/xhtml.html). Essentially, it is a stricter version of HTML that follows the syntax of XML, which appeals to me as I do want my HTML to be well-formed and for browsers to reject it if it's not. For more information about XHTML, I would highly recommend taking a look at the incredibly detailed and well-written guide by Nayuki titled "[Practical guide to XHTML](https://www.nayuki.io/page/practical-guide-to-xhtml)".

## Where are your CSS classes referenced in your tags?

Nowhere! While I gushed about XHTML, I'm not a big fan of CSS. That's not to say that I don't think it has a place on the web (although after seeing infamous websites such as [motherfuckingwebsite.com](http://motherfuckingwebsite.com/), I certainly came close to not using it, lol), I enjoy just focusing on semantic HTML and let classless CSS frameworks do the work for me. This also allows me to replace the current classless CSS framework that I'm using with any other and not have to modify my HTML to accomodate it.
